FROM buildkite/puppeteer:latest
#ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true
ENV PATH="${PATH}:/node_modules/.bin"

EXPOSE 6400

WORKDIR /usr/app
VOLUME /usr/app
RUN npm install -g tsconfig-paths
RUN npm install -g typescript
RUN npm install -g ts-node

copy . .

CMD ["/bin/bash","-c","chmod +x /usr/app/start.sh && /usr/app/start.sh"]
