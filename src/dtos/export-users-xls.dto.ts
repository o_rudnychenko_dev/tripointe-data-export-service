import { ApiPropertyOptional } from '@nestjs/swagger';
import { UserToExportDto } from './user-to-export.dto';

export class ExportUsersXlsDto {
  @ApiPropertyOptional()
  header?: string[][];

  @ApiPropertyOptional()
  rows?: UserToExportDto[];

  @ApiPropertyOptional()
  multipage: boolean;

  @ApiPropertyOptional()
  sheets: any;
}
