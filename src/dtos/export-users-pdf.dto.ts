import { ApiPropertyOptional } from '@nestjs/swagger';
import { UserToExportDto } from './user-to-export.dto';

export class UsersExportPdfDto {
  @ApiPropertyOptional()
  requestUser: string;

  @ApiPropertyOptional()
  builderLogo?: string;

  @ApiPropertyOptional()
  endpoint?: string;

  @ApiPropertyOptional()
  reportTitle?: string;

  @ApiPropertyOptional({
    type: UserToExportDto,
    isArray: true,
  })
  users?: UserToExportDto;
}
