import { ApiPropertyOptional } from '@nestjs/swagger';

export class UserToExportDto {
  @ApiPropertyOptional()
  lastName?: string | null;

  @ApiPropertyOptional()
  firstName?: string | null;

  @ApiPropertyOptional()
  email?: string | null;

  @ApiPropertyOptional()
  phone?: string | null;

  @ApiPropertyOptional()
  jobTitle?: string | null;

  @ApiPropertyOptional()
  userRole?: string | null;

  @ApiPropertyOptional()
  toursCount?: number | null;

  @ApiPropertyOptional()
  createdAt?: Date | null;

  @ApiPropertyOptional()
  updatedAt?: Date | null;

  @ApiPropertyOptional()
  id?: string | null;

  @ApiPropertyOptional()
  termsAccepted?: boolean | null;

  @ApiPropertyOptional()
  ekataScore?: number | null;

  @ApiPropertyOptional()
  verificationMethod?: string | null;
}
