import { Controller, Get } from '@nestjs/common';
import { AppService } from '../services/app.service';
import { MustacheService } from '../services/mustache.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('App controller')
@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly mustacheService: MustacheService,
  ) {}

  @Get()
  async getHello(): Promise<any> {
    const htmlContent = await this.mustacheService.render('users', {
      azaz: 'hello',
    });

    return htmlContent;
  }
}
