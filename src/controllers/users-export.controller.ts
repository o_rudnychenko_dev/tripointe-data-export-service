import { Body, Controller, Header, Post } from '@nestjs/common';
import { UsersExportXlsService } from '../services/users-export-xls.service';
import { UsersExportPdfDto } from '../dtos/export-users-pdf.dto';
import { ExportUsersXlsDto } from '../dtos/export-users-xls.dto';
import { ExportPdfService } from '../services/export-pdf.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Users export controller')
@Controller('users-export')
export class UsersExportController {
  constructor(
    private readonly usersExportXlsService: UsersExportXlsService,
    private readonly exportPdfService: ExportPdfService,
  ) {}

  @Header('Content-Type', 'application/pdf')
  @Post('pdf')
  async exportUsersToPDF(@Body() userData: UsersExportPdfDto) {
    const pdfContent = await this.exportPdfService.generatePDF(
      'users',
      userData,
    );

    return pdfContent;
  }

  @Header(
    'Content-Type',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  )
  @Post('xls')
  async exportUsersToXLS(@Body() userData: ExportUsersXlsDto) {
    const xlsxContent = await this.usersExportXlsService.exportUsersToXlsx(
      userData,
    );

    return xlsxContent;
  }
}
