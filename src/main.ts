import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { ValidationPipe } from '@nestjs/common';
import { join } from 'path';
import * as config from 'config';

async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter(),
  );

  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      whitelist: true,
      skipUndefinedProperties: true,
      skipNullProperties: true,
    }),
  );

  // if (process.env.NODE_ENV !== 'production') {
  //   allowedOrigins.push(new RegExp('(https?://localhost.*):(\\d*)'));
  // }

  const staticDirPath = config.get('staticDirPath');

  app.useStaticAssets({
    root: join(__dirname, '..', 'staticDirPath'),
    prefix: `/${staticDirPath}/`,
  });
  const allowedOrigins = [new RegExp('(https?://tripointe-cms-development.*)')];

  if (process.env.NODE_ENV !== 'production') {
    allowedOrigins.push(new RegExp('(https?://localhost.*):(\\d*)'));
  }

  app.enableCors({
    origin: allowedOrigins,
    methods: ['GET', 'HEAD', 'PUT', 'PATCH', 'POST', 'DELETE', 'OPTIONS'],
  });

  const options = new DocumentBuilder()
    .setTitle('Tripointe data export service')
    .setDescription('Tripointe data export service api documentation')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('docs', app, document);

  console.log('applicationPort', config.get('applicationPort'));
  await app.listen(config.get('applicationPort') || 3000, '0.0.0.0');
}
bootstrap();
