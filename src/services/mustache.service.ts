import { Injectable, OnModuleInit } from '@nestjs/common';
import { join } from 'path';
import * as fs from 'fs';
import * as config from 'config';
import * as Mustache from 'mustache';

@Injectable()
export class MustacheService implements OnModuleInit {
  private readonly staticFilesUrl = `${config.get(
    'baseUrl',
  )}:${config.get<number>('applicationPort')}/${config.get('staticDirPath')}`;
  private readonly mustache = Mustache;
  private readonly templatesDir = join(__dirname, '../..', 'views');
  private readonly templatesStorage: Map<string, string> = new Map<
    string,
    string
  >();

  async onModuleInit(): Promise<any> {
    this.loadTemplates();
  }

  private loadTemplates(): void {
    const templateDirFilesRef = fs.readdirSync(this.templatesDir);
    for (const fileRef of templateDirFilesRef) {
      const filePath = join(this.templatesDir, fileRef);
      if (!fs.lstatSync(filePath).isDirectory()) {
        const [templateName] = fileRef.split('.html');
        const templateContent = fs.readFileSync(filePath).toString('utf-8');
        this.templatesStorage.set(templateName, templateContent);
        this.mustache.parse(templateContent);
      }
    }
  }

  render(templateFileName: string, view: any): any {
    if (this.templatesStorage.has(templateFileName)) {
      const template = this.templatesStorage.get(templateFileName);
      return this.mustache.render(template, {
        staticFilesUrl: this.staticFilesUrl,
        ...view,
      });
    }
  }
}
