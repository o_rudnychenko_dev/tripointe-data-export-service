import { Injectable } from '@nestjs/common';
import { BrowserContext } from 'puppeteer';
import { InjectContext } from 'nest-puppeteer';
import { MustacheService } from './mustache.service';

@Injectable()
export class ExportPdfService {
  constructor(
    @InjectContext('ChromeInstance') private readonly browser: BrowserContext,
    private readonly mustacheService: MustacheService,
  ) {}

  async generatePDF(templateName: string, data: any): Promise<any> {
    const page = await this.browser.newPage();
    const htmlContent = this.mustacheService.render(templateName, data);
    await page.setContent(htmlContent);
    const pdfContent = await page.pdf();
    await page.close();
    return { content: pdfContent };
  }
}
