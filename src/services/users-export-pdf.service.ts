import { Injectable } from '@nestjs/common';
import { ExportPdfService } from './export-pdf.service';

@Injectable()
export class UsersExportPdfService {
  constructor(private readonly exportPdfService: ExportPdfService) {}
}
