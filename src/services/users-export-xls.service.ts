import { Injectable } from '@nestjs/common';
import XLSX from 'xlsx';
import { ExportUsersXlsDto } from '../dtos/export-users-xls.dto';

@Injectable()
export class UsersExportXlsService {
  private readonly xlsx = XLSX;

  async exportUsersToXlsx(data: ExportUsersXlsDto): Promise<Buffer> {
    const wb = this.xlsx.utils.book_new();
    if (
      data.hasOwnProperty('multipage') &&
      data.multipage === true &&
      data.hasOwnProperty('sheets') &&
      data.sheets.length !== 0
    ) {
      for (const sheetIndex in data.sheets) {
        if (!data.sheets[sheetIndex].hasOwnProperty('name')) {
          data.sheets[sheetIndex].name = `DATA${sheetIndex}`;
        }
        await this.addSheet(wb, data.sheets[sheetIndex]);
      }
    } else {
      await this.addSheet(wb, data);
    }

    return this.xlsx.write(wb, {
      type: 'buffer',
      bookType: 'xlsx',
    });
  }

  private async addSheet(workbook, sheetData): Promise<void> {
    return new Promise((resolve, reject) => {
      const sheetHeaders = {};
      sheetData.header.forEach((col) => {
        sheetHeaders[col[0]] = col[1];
      });

      sheetData.rows.splice(0, 0, sheetHeaders);

      const ws = this.xlsx.utils.json_to_sheet(sheetData.rows, {
        header: sheetData.header.map((col) => col[0]),
        skipHeader: true,
      });

      console.log(ws);

      this.xlsx.utils.book_append_sheet(
        workbook,
        ws,
        sheetData.hasOwnProperty('name') && sheetData.name.length !== 0
          ? sheetData.name
          : 'DATA',
      );
      resolve();
    });
  }
}
