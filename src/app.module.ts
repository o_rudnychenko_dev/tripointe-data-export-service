import { Module } from '@nestjs/common';
import { AppController } from './controllers/app.controller';
import { AppService } from './services/app.service';
import { UsersExportXlsService } from './services/users-export-xls.service';
import { UsersExportController } from './controllers/users-export.controller';
import { MustacheService } from './services/mustache.service';
import { PuppeteerModule } from 'nest-puppeteer';
import { UsersExportPdfService } from './services/users-export-pdf.service';
import { ExportPdfService } from './services/export-pdf.service';

@Module({
  imports: [
    PuppeteerModule.forRoot(
      {
        pipe: true,
        headless: true,
        // executablePath: '/usr/bin/chromium-browser',
        args: [
          `--disable-dev-shm-usage`,
          '--no-sandbox',
          '--disable-setuid-sandbox',
        ],
      },
      'ChromeInstance',
    ),
    PuppeteerModule.forFeature(),
  ],
  controllers: [AppController, UsersExportController],
  providers: [
    AppService,
    MustacheService,
    ExportPdfService,
    UsersExportXlsService,
    UsersExportPdfService,
  ],
})
export class AppModule {}
